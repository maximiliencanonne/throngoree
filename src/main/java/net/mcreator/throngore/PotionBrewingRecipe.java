
package net.mcreator.throngore;

import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.common.brewing.BrewingRecipeRegistry;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;

import net.mcreator.throngore.item.FasterItem;

@ThrongoreModElements.ModElement.Tag
public class PotionBrewingRecipe extends ThrongoreModElements.ModElement {
	public PotionBrewingRecipe(ThrongoreModElements instance) {
		super(instance, 24);
	}

	@Override
	public void init(FMLCommonSetupEvent event) {
		BrewingRecipeRegistry.addRecipe(Ingredient.fromStacks(new ItemStack(Items.GLASS_BOTTLE, (int) (1))),
				Ingredient.fromStacks(new ItemStack(FasterItem.block, (int) (1))), new ItemStack(Items.WITHER_SKELETON_SKULL, (int) (1)));
	}
}
