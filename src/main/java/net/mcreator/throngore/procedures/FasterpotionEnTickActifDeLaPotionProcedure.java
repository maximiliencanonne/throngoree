package net.mcreator.throngore.procedures;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.Entity;

import net.mcreator.throngore.ThrongoreModElements;
import net.mcreator.throngore.ThrongoreMod;

import java.util.Map;

@ThrongoreModElements.ModElement.Tag
public class FasterpotionEnTickActifDeLaPotionProcedure extends ThrongoreModElements.ModElement {
	public FasterpotionEnTickActifDeLaPotionProcedure(ThrongoreModElements instance) {
		super(instance, 23);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				ThrongoreMod.LOGGER.warn("Failed to load dependency entity for procedure FasterpotionEnTickActifDeLaPotion!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof PlayerEntity) {
			((PlayerEntity) entity).abilities.allowFlying = (true);
			((PlayerEntity) entity).sendPlayerAbilities();
		}
		if (entity instanceof PlayerEntity) {
			((PlayerEntity) entity).abilities.disableDamage = (true);
			((PlayerEntity) entity).sendPlayerAbilities();
		}
	}
}
